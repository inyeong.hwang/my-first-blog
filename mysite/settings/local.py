from .base import *
import os, json

secret_file = os.path.join(secret_file_path, "local_secrets.json")

# BASE_DIR = Path(__file__).resolve().parent
with open(secret_file) as f:
    secrets = json.loads(f.read())

DEBUG = True

ALLOWED_HOSTS = [] #"127.0.0.1"

DATABASES = secrets["DB_INFO"]

SECRET_KEY = secrets["SECRET_KEY"]

# 로깅설정=default